/*
 * This file is part of Pic4Carto.
 * 
 * Pic4Carto is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * Pic4Carto is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with Pic4Carto.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * EventLauncher is a class able to launch events on its name. You can listen to events using {@link #EventLauncher#on} method, and trigger event using {@link #EventLauncher#fire} method.
 */
class EventLauncher {
//CONSTRUCTOR
	/**
	 * This class must be extended and is not intended to be instantiated on its own.
	 * @return {EventLauncher} this
	 * @abstract
	 */
	constructor() {
		if(this.constructor.name === "EventLauncher") {
			throw new TypeError("ctrl.eventlauncher.cantinstantiate");
		}
		
		this.eventListeners = {};
		
		return this;
	}

//OTHER METHODS
	/**
	 * Add a new listener on a given event.
	 * @param {string} event The event to listen to.
	 * @param {function} handler The handling function, which will be called when event is fired.
	 * @return {EventLauncher} this
	 */
	on(event, handler) {
		if(this.eventListeners[event] == undefined) {
			this.eventListeners[event] = [];
		}
		this.eventListeners[event].push(handler);
		
		return this;
	}
	
	/**
	 * Removes a listener of an event.
	 * @param {string} event The concerned event.
	 * @param {function} [handler] The handler to remove. If no handler is given, all handlers for this events are removed.
	 * @return {EventLauncher} this
	 */
	off(event, handler) {
		if(handler == null) {
			this.eventListeners[event] = undefined;
		}
		else if(this.eventListeners[event] != undefined) {
			while(this.eventListeners[event].indexOf(handler) >= 0) {
				this.eventListeners[event].splice(this.eventListeners[event].indexOf(handler), 1);
			}
		}
		
		return this;
	}
	
	/**
	 * Fires an event. It calls all listeners associated to the given event, passing the given data as argument.
	 * @param {string} event The event to fire.
	 * @param {object} [data] The associated data.
	 * @return {EventLauncher} this
	 */
	fire(event, data) {
		for(let f in this.eventListeners[event]) {
			setTimeout(() => {
				this.eventListeners[event][f](data);
			}, 0);
		}
		
		return this;
	}
}

module.exports = EventLauncher;
