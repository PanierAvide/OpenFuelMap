/*
 * This file is part of OpenFuelMap.
 *
 * OpenFuelMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * OpenFuelMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OpenFuelMap.  If not, see <http://www.gnu.org/licenses/>.
 */

let DataManager = require("./DataManager");
let MainView = require("../view/Main");

/**
 * Main controller manages everything related to main page.
 * It initializes the view and DataManager.
 */
class Main {
//CONSTRUCTOR
	constructor(divId) {
		this.data = null;

		//Services init
		this.dataManager = new DataManager();

		this.dataManager.on("stationsready", s => {
			this.view.wAlert.hide();
			this.data = s;

			this.view.setStations(s);
		});

		this.dataManager.on("fuelschanged", f => {
			this.dataManager.getLastFuel().then(previousFuel => this.view.setFuels(f, previousFuel))
		});

		this.dataManager.on("stationsfailed", (status, text) => {
			this.view.wAlert.hide();
			this.view.wAlert.show("Download failed", "error", "exclamation-circle", 3000);
			this.data = null;
			this.view.setFuels(null);
			this.view.setStations(null);
			console.error("Stations data retrieval failed: "+status+" "+text);
		});

		//View init
		this.view = new MainView(divId);
		this.view.wAlert.show("Zoom in to see stations", "info", "search-plus", 120000);

		//Handle view events
		this.view.wStationMap.on("mapmoved", () => {
			this.view.wAlert.hide();
			this.view.wAlert.show("Loading", "info", "spinner", 120000, "pulse");
			this.dataManager.getStationsData(this.view.wStationMap.getBounds());
		});

		this.dataManager.on("attributionchanged", atr => {
			if(this._prevAttrib) {
				this.view.wStationMap.map.attributionControl.removeAttribution(this._prevAttrib);
			}
			this._prevAttrib = "Data &copy; "+atr;
			this.view.wStationMap.map.attributionControl.addAttribution(this._prevAttrib);
		});

		this.view.wStationMap.on("fuelselected", f => {
			this.dataManager.saveLastSetting("fuel", f)
		});

		this.view.wStationMap.on("setSetting", ({name, value}) => {
			this.dataManager.saveLastSetting(name, value)
		})
		this.view.wStationMap.on("getSettings", ([names, cb]) => {
			Promise.all(names.map(name => {
				return this.dataManager.getLastSetting(name)
			})).then(settings => {
				console.log({names, settings})
				cb(names.map((name, i) => {
					return { name, value: settings[i]}
				}).reduce((a, {name, value}) => {
					return {...a, [name]: value}
				}, {}))
			})
		})

		this.dataManager.getFuels();
	}
}

module.exports = {
	init: function(divId) {
		return new Main(divId);
	}
};
