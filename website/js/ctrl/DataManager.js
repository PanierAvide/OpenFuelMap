/*
 * This file is part of OpenFuelMap.
 *
 * OpenFuelMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * OpenFuelMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OpenFuelMap.  If not, see <http://www.gnu.org/licenses/>.
 */

let EventLauncher = require("./EventLauncher");
let Station = require("../model/Station");
const localforage = require("localforage");

const OWN_DATA_URL = "/stations";

/**
 * DataManager role is to retrieve data from external sources, in particular OpenEventDb.
 */
class DataManager extends EventLauncher {
//CONSTRUCTOR
	constructor() {
		super();
	}


//OTHER METHODS
	/**
	 * Start the retrieval of stations data from OpenEventDb.
	 * To get the retrieved data, you must listen to stationsready event.
	 * @param {L.LatLngBounds} bbox The bounding box of the area.
	 */
	getStationsData(bbox) {
		this.ajax(
			OWN_DATA_URL+"?bbox="+bbox.getSouth()+","+bbox.getWest()+","+bbox.getNorth()+","+bbox.getEast(),
			"json",
			d => {
				if(d.status === "failed") {
					this.fire("stationsfailed");
				}
				else {
					if(d.status === "partial") {
						console.log("Failures from certain fetchers");
					}

					const data = [];

					for(let f of d.features) {
						try {
							data.push(new Station(f));
						}
						catch(e) {
							console.error(e);
						}
					}

					this.fire("stationsready", data);
					this.fire("attributionchanged", d.attribution);
				}
			},
			() => { this.fire("stationsfailed"); }
		);
	}

	getFuels() {
		this.ajax('/fuels', 'json', d=> {
			this.fire("fuelschanged", d.fuels); // fuel is needed to filter stations
		})
	}

	getLastFuel() {
		return localforage.getItem("fuel")
	}
	getLastSetting(name) {
		return localforage.getItem(name)
	}

	saveLastSetting(name, value) {
		return localforage.setItem(name, value)
	}

	/**
	 * Creates an Ajax GET request.
	 * @param {string} url The URL of the data to retrieve.
	 * @param {string} type The expected file format.
	 * @param {function} success The callback when file is successfully retrieved.
	 * @param {function} [fail] The callback when file retrieval fails.
	 * @private
	 */
	ajax(url, type, success, fail) {
		fail = fail || (() => { throw new Error("datamanager.ajax.fail"); });

		let xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = () => {
			if(xmlhttp.readyState === XMLHttpRequest.DONE) {
				if(xmlhttp.status === 200) {
					if(type == "json") {
						success(JSON.parse(xmlhttp.responseText));
					}
					else {
						success(xmlhttp.responseText);
					}
				}
				else {
					fail(xmlhttp.status, xmlhttp.responseText);
				}
			}
		};
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}
}

module.exports = DataManager;
