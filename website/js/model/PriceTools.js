
function priceOld(currentPrice) {
    return currentPrice && currentPrice.maj ? (new Date(currentPrice.maj).getTime() - new Date()) / (1000 * 24 * 60 * 60) : null
}

function priceAgeColor(currentPrice) {
    const priceAge = priceOld(currentPrice)
    return priceAge ? (priceAge > -2) ? "green" : (priceAge > -7) ? "orange" : "red" : "black"
}

module.exports = { priceOld, priceAgeColor }