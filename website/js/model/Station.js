/*
 * This file is part of OpenFuelMap.
 *
 * OpenFuelMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * OpenFuelMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OpenFuelMap.  If not, see <http://www.gnu.org/licenses/>.
 */

let L = require("leaflet");
const PriceTools = require("./PriceTools")

/**
 * A station is a place where one can buy fuel at given prices. It offers some services.
 */
class Station {
//CONSTRUCTOR
	/**
	 * Class constructor. Used for normalizing OpenEventDb data.
	 * @param {Object} options The options to create this station.
	 * @throws {Error} Options are not recognized
	 */
	constructor(options) {
		if(!options) {
			throw new Error("model.station.missing.options");
		}

		this.brand = options.properties.brand || "Marque inconnue";
		this.name = options.properties.name || options.properties.brand || "Station-service";
		this.coordinates = L.GeoJSON.coordsToLatLng(options.geometry.coordinates);
		this.osmId = options.properties.osm_id;

		//Read prices
		this.prices = options.properties.prices;

		//Read services
		this.services = options.properties.services;
	}

	price(fuel) {
		return this.prices[fuel] && this.prices[fuel].valeur;
	}

	priceAge(fuel) {
		return this.prices[fuel] && PriceTools.priceOld(this.prices[fuel]);
	}
}

module.exports = Station;
