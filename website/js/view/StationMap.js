/*
 * This file is part of OpenFuelMap.
 *
 * OpenFuelMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * OpenFuelMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OpenFuelMap.  If not, see <http://www.gnu.org/licenses/>.
 */

let EventLauncher = require("../ctrl/EventLauncher");
let L = require("leaflet");
require("leaflet-hash");
require("leaflet-geosearch");
require("leaflet-geosearch/src/js/l.geosearch.provider.openstreetmap");
require("leaflet.locatecontrol");
require("leaflet-easybutton");
let Rainbow = require("rainbowvis.js");
let PriceTools = require("../model/PriceTools");


//CONSTANTS
const MIN_DATA_ZOOM = 10;

/**
 * Station map displays station location over a dynamic map.
 */
class StationMap extends EventLauncher {
//CONSTRUCTOR
	/**
	 * Class constructor.
	 * @param {string} divId The DIV ID where the view must be created
	 */
	constructor(divId) {
		super();

		//Map init
		this.dom = document.getElementById(divId);
		this.dom.className += " ofm-station-map";
		this.map = L.map(divId).setView([46.43, 2.41], 6);
		this.map.attributionControl.setPrefix('<a href="http://pavie.info">&copy; Adrien Pavie 2019</a> | <a href="./carburant-bon-marche.html">À propos</a>');
		let hash = new L.Hash(this.map);

		//Tiles
		L.tileLayer('//server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}', {
			maxZoom: 19,
			attribution:
				'Tiles &copy; Esri &mdash; ' +
				'Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community'
			,
		}).addTo(this.map);

		this.dataAge = false;

		//Locate
		this.mapLocate = L.control.locate({ locateOptions: { maxZoom: 13 } }).addTo(this.map);

		//Search widget
		this.mapSearch = new L.Control.GeoSearch({
			provider: new L.GeoSearch.Provider.OpenStreetMap(),
			position: 'topleft',
			showMarker: false,
			retainZoomLevel: false
		}).addTo(this.map);

		//Stations data
		this.stations = [];
		this.stationsLayer = null;


		//Create legend for station colors
		L.Control.Legend = L.Control.extend({
			onAdd(map) {
				this.colorGradient =
					{
						green: new Rainbow().setSpectrum("green", "orange"),
						red: new Rainbow().setSpectrum("orange", "red", "violet")
					}

				this.domLegend = document.createElement("div");
				this.domLegend.className = "ofm-station-map-legend hidden";
				return this.domLegend
			},
			hide() {
				this.domLegend.classList.add("hidden")
			},
			/**
			 * Creates the DOM for color legend.
			 */
			setLegend(fuel, stations) {
				if (!stations || stations.length === 0) {
					return;
				}

				const allPrices = stations.map(s => s.price(fuel)).filter(Boolean).sort()
				if (allPrices.length === 0) {
					return;
				}

				let decilePrice = (allPrices.slice(0, allPrices.length / 10)).slice(-1)[0] || allPrices[0]

				let minPrice = Math.min.apply(null, allPrices);
				let maxPrice = Math.max.apply(null, allPrices);

				if(minPrice === maxPrice) { maxPrice += 0.01; }
				if(minPrice === decilePrice) { decilePrice += 0.01; }
				if(decilePrice >= maxPrice) { maxPrice = decilePrice+0.01; }

				this.colorGradient.green.setNumberRange(minPrice, decilePrice);
				this.colorGradient.red.setNumberRange(decilePrice, maxPrice);
				this.colorGradient.decilePrice = decilePrice

				this.domLegend.innerHTML = "";

				if(minPrice && maxPrice) {
					let legendWrap = document.createElement("div");
					legendWrap.className = "ofm-station-map-legend-wrapper";

					let colorRamp = document.createElement("div");
					colorRamp.className = "ofm-station-map-legend-colors";

					let priceRamp = document.createElement("div");
					priceRamp.className = "ofm-station-map-legend-prices";

					const prices =
						[ minPrice, decilePrice,maxPrice]
							.map(v => v.toFixed(2))
							.map(v => `${v} €&nbsp;`)
							.filter(Boolean);

					const gradient =
						[
							["green", 0],
							["orange", 50],
							["red", 75],
							["violet", 100],
						].map(([a, b]) => `${a} ${b.toFixed(0)}%`)
							.join(', ')

					colorRamp.style.background = `linear-gradient(90deg, ${gradient})`
					colorRamp.innerHTML = '&nbsp;'

					for(let p of prices) {
						//Price
						let price = document.createElement("span");
						price.innerHTML = p;
						priceRamp.appendChild(price);
					}

					let legendTitle = document.createElement("h2");
					legendTitle.className = "ofm-station-map-legend-title";
					legendTitle.innerText = `Color legend`

					legendWrap.appendChild(legendTitle);
					legendWrap.appendChild(priceRamp);
					legendWrap.appendChild(colorRamp);
					this.domLegend.appendChild(legendWrap)
					this.domLegend.classList.remove("hidden")
				}
			},
			getColor(currentPrice) {
				return currentPrice && currentPrice.valeur ? ("#" +(currentPrice.valeur < this.colorGradient.decilePrice ? this.colorGradient.green.colourAt(currentPrice.valeur) : this.colorGradient.red.colourAt(currentPrice.valeur))) : 'darkgrey';
			}

		})


		L.control.legend = function (opts) {
			return new L.Control.Legend(opts)
		}

		L.Control.PopupBanner = L.Control.extend({
			case: 1,
			template: `
				<div class="bannerContent">
					<div class="template1">
						<label for="fuel-selector">Choose your fuel type</label> 
						<select class="fuel-selector" id="fuel-selector">
							<option></option>
						</select>
						<div>
							<label for="dataage">Show data age</label>
							<input type="checkbox" id="dataage" value="on">
						</div>
						<div>
							<label for="noPriceFilter">Hide no price</label>
							<input type="checkbox" id="noPriceFilter" value="on">
						</div>
						<div>
							<label for="cheapFilter">Keep cheap (1<sup>st</sup> decile)</label>
							<input type="checkbox" id="cheapFilter" value="on">
						</div>
						<div>
							<label for="youngestFilter">Keep youngest (<2d)</label>
							<input type="checkbox" id="youngestFilter" value="on">
						</div>
						<button class="close_banner">Close</button>
					</div>
					<div class="template2">
						<a href="" class="fa fa-sliders"></a>
					</div>
				</div>
			`,
			onAdd: function(map) {
				this.popup = new DOMParser().parseFromString(this.template, "text/html").documentElement.querySelector(".bannerContent")
				this.fuelSelector = this.popup.querySelector(".fuel-selector")
				this.showDataAge = this.popup.querySelector("#dataage")
				this.cheapFilter = this.popup.querySelector("#cheapFilter")
				this.noPriceFilter = this.popup.querySelector("#noPriceFilter")
				this.youngestFilter = this.popup.querySelector("#youngestFilter")

				this.popup.querySelector(".close_banner").addEventListener("click", this.toggle.bind(this));
				this.popup.querySelector("a.fa").addEventListener("click", this.toggle.bind(this));

				return this.popup;
			},

			setFuel(newFuel) {
				this.fuelSelector.value = newFuel
			},

			setFuels(fuels) {
				fuels.forEach(fuel => {
					let fuelDom = document.createElement("option");
					fuelDom.innerHTML = fuel;
					fuelDom.setAttribute('value', fuel)
					this.fuelSelector.appendChild(fuelDom)
				});
			},

			toggle: function(e) {
				e.stopPropagation()
				e.preventDefault()
				this.popup.classList.toggle(`collapsed`)
				this.popup.classList.toggle(`leaflet-bar`)
			},
			close: function() {
				this.popup.classList.add(`collapsed`)
				this.popup.classList.add(`leaflet-bar`)
			},
		});

		L.control.popupBanner = function (opts) {
			return new L.Control.PopupBanner(opts)
		}

		this.legend = L.control.legend({position: 'bottomright'}).addTo(this.map)

		this.popupBanner = L.control.popupBanner({position: 'bottomright'}).addTo(this.map);

		//Bind map events
		this.map.on("load moveend", () => {
			//Check zoom level
			if (this.map.getZoom() >= MIN_DATA_ZOOM) {
				this.fire("mapmoved", this.getBounds());
			} else {
				this.fire('insufficientzoom')
			}
		});
		this.popupBanner.fuelSelector.addEventListener("change", (e) => {
			this.fire("fuelselected", e.target.value)
		})
		this.popupBanner.showDataAge.addEventListener("click", () => {
			this.showDataAge = !this.showDataAge;
			this.fire("setSetting", {"name": "showDataAge", value: this.showDataAge})
			this.setData(this.stations);

			this.map.doubleClickZoom.disable()
			setTimeout(() => {
				this.map.doubleClickZoom.enable()
			}, 300)
		})

		this.popupBanner.cheapFilter.addEventListener("click", () => {
			this.cheapFilter = !this.cheapFilter;
			this.fire("setSetting", {"name": "cheapFilter", value: this.cheapFilter})
			this.setData(this.stations);

			this.map.doubleClickZoom.disable()
			setTimeout(() => {
				this.map.doubleClickZoom.enable()
			}, 300)
		})
		this.popupBanner.noPriceFilter.addEventListener("click", () => {
			this.noPriceFilter = !this.noPriceFilter;

			this.fire("setSetting", {"name": "noPriceFilter", value: this.noPriceFilter})
			this.setData(this.stations);

			this.map.doubleClickZoom.disable()
			setTimeout(() => {
				this.map.doubleClickZoom.enable()
			}, 300)
		})
		this.popupBanner.youngestFilter.addEventListener("click", () => {
			this.youngestFilter = !this.youngestFilter;
			this.fire("setSetting", {"name": "youngestFilter", value: this.youngestFilter})
			this.setData(this.stations);

			this.map.doubleClickZoom.disable()
			setTimeout(() => {
				this.map.doubleClickZoom.enable()
			}, 300)
		})

		const mutationObserver = new MutationObserver(_ => {
			this.fire("setSetting", {name: "popupBanner", value: this.popupBanner.popup.classList.contains("collapsed")})
		})
		mutationObserver.observe(this.popupBanner.popup, {attributeFilter: ["class"]})

		setTimeout(() => {
			this.fire("getSettings",
				[
					["showDataAge"
						, "cheapFilter"
						, "noPriceFilter"
						, "youngestFilter"
						, "popupBanner"]
					, this.initSettings.bind(this)
				])
		},100)
	}

	initSettings({showDataAge, cheapFilter, noPriceFilter, youngestFilter, popupBanner}) {
		this.popupBanner.showDataAge.checked = showDataAge
		this.popupBanner.cheapFilter.checked = cheapFilter
		this.popupBanner.noPriceFilter.checked = noPriceFilter
		this.popupBanner.youngestFilter.checked = youngestFilter
		this.cheapFilter = cheapFilter
		this.showDataAge = showDataAge
		this.noPriceFilter = noPriceFilter
		this.youngestFilter = youngestFilter
		if(popupBanner) {
			this.popupBanner.close()
		}
	}

	getBounds() {
		const bounds = this.map.getBounds()
		const northWest = this.map.latLngToLayerPoint(bounds.getNorthWest())
		const southEast = this.map.latLngToLayerPoint(bounds.getSouthEast())

		let legends = document.querySelector(".leaflet-control-attribution");
		const legendsBounding = legends?.getBoundingClientRect();
		const parentBounding = legends?.offsetParent?.getBoundingClientRect()

		const widthControls = 32
		const padding = 10
		const topOfLegendOnDesktop = 0

		const soutEastY = southEast.y - ((parentBounding?.bottom || topOfLegendOnDesktop) - (legendsBounding?.top || 0))

		const newBounds = L.latLngBounds(
			this.map.layerPointToLatLng([northWest.x + widthControls, northWest.y + padding]),
			this.map.layerPointToLatLng([southEast.x - padding, soutEastY - padding])
		)

		// if (this.active) {
		// 	this.map.removeLayer(this.active)
		// }
		// this.active = L.rectangle(newBounds, {color: "#ff7800", weight: 1})
		// this.map.addLayer(this.active)

		return newBounds
	}

//MODIFIERS
	/**
	 * Change the stations shown on map.
	 * @param {Station[]} stations The stations to display
	 */
	setData(stations) {
		this.clearPreviousLayer();
		this.stationsLayer = L.layerGroup();

		this.stations = stations
			.sort((a, b) => a.price(this.fuel) - b.price(this.fuel) );
		this.legend.setLegend(this.fuel, stations);

		function filteredStation(filter, predicate, allStations ) {
			if (filter) {
				return allStations
					.filter(predicate)

			} else {
				return allStations
			}
		}

		const stationDisplay = filteredStation(this.cheapFilter, s => {
			return s.price(this.fuel) <= this.legend.colorGradient.decilePrice
		}, filteredStation(this.noPriceFilter, s => {
			return s.price(this.fuel)
		}, filteredStation(this.youngestFilter, s => {
			return s.priceAge(this.fuel) > -2
		}, this.stations)))
		stationDisplay
			.reverse()// cheapest poi on top of others
			.map(s => this.createStationLayer(s))
			.forEach(l => this.stationsLayer.addLayer(l));

		this.stationsLayer.addTo(this.map);

		this.fire('stationsMapped', this.stations.length)
	}

	/**
	 * Change the fuel to use for stations color
	 * @param {string} fuel The fuel to use
	 * @param {boolean} [reload] True if data must be updated
	 */
	setFuel(fuel, reload) {
		this.fuel = fuel;
		if(reload) { // fuel changed so, refilter station on this fuel
			this.setData(this.stations);
		}
	}

	/**
	 * Empties and delete previous stations layer.
	 */
	clearPreviousLayer() {
		if(this.stationsLayer != null) {
			this.stationsLayer.clearLayers();
			this.map.removeLayer(this.stationsLayer);
			this.stationsLayer = null;
			this.legend.hide()
		}
	}

	computeBorder(currentPrice, color) {
		if (this.showDataAge) {
			return PriceTools.priceAgeColor(currentPrice);
		} else {
			return color;
		}
	}

	/**
	 * Create the leaflet layer for a given station.
	 * @param {Station} station The station
	 * @return {L.Layer} The leaflet layer for this station
	 * @private
	 */
	createStationLayer(station) {
		let currentPrice = station.prices[this.fuel]
		let color = this.legend.getColor(currentPrice)

		let layer = L.circleMarker(station.coordinates, {
			radius: 8,
			color: this.computeBorder(currentPrice, color),
			fillColor: color,
			opacity: 0.7,
			fillOpacity: 0.8
		});

		layer.on("click", () => {
			this.fire("stationclicked", station);
		});

		return layer;
	}
}

module.exports = StationMap;
