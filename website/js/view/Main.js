/*
 * This file is part of OpenFuelMap.
 * 
 * OpenFuelMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OpenFuelMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OpenFuelMap.  If not, see <http://www.gnu.org/licenses/>.
 */

let Alert = require("./Alert");
let StationDetails = require("./StationDetails");
let StationMap = require("./StationMap");
const localforage = require("localforage")


/**
 * Main view creates DOM structure and initializes all views.
 */
class Main {
//CONSTRUCTOR
	/**
	 * Class constructor.
	 * @param {string} divId The DIV ID where the view must be created
	 */
	constructor(divId) {
		//Clean DIV
		this.dom = document.getElementById(divId);
		this.dom.innerHTML = "";
		this.dom.className = "ofm-main";
		
		//Create children nodes
		this.domFuelSelector = document.createElement("div");
		this.domFuelSelector.id = divId+"-fuel-selector";
		
		this.domStationDetails = document.createElement("div");
		this.domStationDetails.id = divId+"-station-details";
		
		this.domStationMap = document.createElement("div");
		this.domStationMap.id = divId+"-station-map";
		
		this.domAlert = document.createElement("div");
		this.domAlert.id = divId+"-alert";
		
		//Insert in DOM
		this.dom.appendChild(this.domStationMap);
		this.dom.appendChild(this.domStationDetails);
		this.dom.appendChild(this.domAlert);
		
		//Init widgets
		this.wAlert = new Alert(this.domAlert.id);
		this.wStationDetails = new StationDetails(this.domStationDetails.id);
		this.wStationMap = new StationMap(this.domStationMap.id);
		
		//Default state: some widgets hidden
		this.domStationDetails.className += " hidden";
		this.domStationMap.style.bottom = 0;
		
		/*
		 * Events
		 */
		this.wStationMap.on("stationclicked", s => {
			this.wStationDetails.set(s);
			this.domStationDetails.className = this.domStationDetails.className.replace("hidden", "").trim();
		});

		this.wStationMap.on('stationsMapped', count => {
			this.wAlert.hide();
			if(count === 0) {
				this.wAlert.show("No matching stations around", "warning", "frown-o", 3000, "spin");
			} else {
				this.wAlert.show(`Mapped ${count} stations`, "info", "info-circle", 3000);
			}
		})

		this.wStationMap.on('mapmoved', bounds => {
			const layers = this.wStationMap.stationsLayer?.getLayers() || [];
			const visibleStations = layers.filter(l =>  {
				try {
					return bounds.contains(l.getLatLng());
				} catch {
					return false;
				}
			})

			this.wAlert.hide()
			if (layers.length !== 0 && visibleStations.length === 0) {
				this.wAlert.show("Zoom out to see cheap stations", "warning", "exclamation-triangle", 120000)
			}
		})

		this.wStationMap.on('insufficientzoom', () => {
			this.wAlert.hide();
			this.wAlert.show("Zoom in to see stations", "info", "search-plus", 120000);
			this.setFuels(null);
			this.setStations(null);
		})

		this.wStationMap.on("fuelselected", f => {
			this.wStationMap.setFuel(f, true);
			this.wStationDetails.setFuel(f, this.wStationDetails.station !== null);

			if(this.wStationDetails.station && this.wStationDetails.station.prices[f] === undefined) {
				this.domStationDetails.className += " hidden";
			}

			this.lastFuel = f;
		})
	}
	
//MODIFIERS
	/**
	 * Updates the stations data.
	 * @param {Station[]} stations The stations data.
	 */
	setStations(stations) {
		if(stations && stations.length > 0) {
			this.wStationMap.setData(stations);
			this.domStationMap.style.bottom = null;
		} else {
			this.wStationMap.clearPreviousLayer();
			this.domStationMap.style.bottom = 0;
		}
		this.wStationMap.map.invalidateSize();
	}
	
	/**
	 * Updates the fuel data.
	 * @param {string[]} fuels The fuels data.
	 * @param previousFuel
	 */
	setFuels(fuels, previousFuel = null) {
		if(fuels) {
			if(!this.lastFuel || fuels.indexOf(this.lastFuel) < 0) {
				this.lastFuel = (previousFuel === "E10" ? "SP95 (E10)" : previousFuel) || ((fuels.indexOf("Gazole") >= 0) ? "Gazole" : fuels[0]);
			}

			this.wStationMap.setFuel(this.lastFuel, false);
			this.wStationDetails.setFuel(this.lastFuel, this.wStationDetails.station !== null);
			this.wStationMap.popupBanner.setFuels(fuels)
			this.wStationMap.popupBanner.setFuel(this.lastFuel)
		}
		else {
			this.domFuelSelector.className += " hidden";
			this.domStationMap.style.bottom = 0;
			this.wStationMap.map.invalidateSize();
		}
	}
}

module.exports = Main;
