/*
 * This file is part of OpenFuelMap.
 * 
 * OpenFuelMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * OpenFuelMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with OpenFuelMap.  If not, see <http://www.gnu.org/licenses/>.
 */

class Alert {
//CONSTRUCTOR
	/**
	 * Class constructor.
	 * @param {string} divId The DIV ID where the view must be created
	 */
	constructor(divId) {
		this.messages = [];
		this.timer = null;
		
		//Div init
		this.dom = document.getElementById(divId);
		this.dom.innerHTML = "";
		this.dom.className = "ofm-alert hidden";
	}

//MODIFIERS
	/**
	 * Adds a message to queue for display.
	 * @param {string} text The text to show
	 * @param {string} [type] The kind of message (info, warning, error). Defaults to info.
	 * @param {string} [icon] The icon to display (using font awesome). Hidden by default.
	 * @param {int} [timeout] The duration of display, in milliseconds. By default 3000ms.
	 * @param {string} [animate] Add an animation to the icon (pulse or spin). Default none.
	 */
	show(text, type, icon, timeout, animate) {
		this.messages.push({
			text: text,
			type: (type == "warning" || type == "error") ? type : "info",
			icon: icon || null,
			timeout: timeout || 3000,
			animate: animate ? "fa-"+animate : ""
		});
		
		//Call display method
		if(this.timer == null) {
			this._update();
		}
	}
	
	/**
	 * Hides the alert and empties the message queue.
	 */
	hide() {
		this.messages = [];
		this.timer = null;
		this.dom.className = "ofm-alert hidden";
	}
	
	/**
	 * Changes the shown message and handles timer.
	 * @private
	 */
	_update() {
		if(this.messages.length > 0) {
			let msg = this.messages.shift();
			this.dom.className = "ofm-alert ofm-alert-"+msg.type;
			this.dom.innerHTML = (msg.icon) ? `<i class="fa fa-${msg.icon} ${msg.animate} fa-lg"></i> ` : "";
			this.dom.innerHTML += msg.text;
			
			this.timer = setTimeout(this._update.bind(this), msg.timeout);
		}
		else {
			this.dom.className = "ofm-alert hidden";
			this.timer = null;
		}
	}
}

module.exports = Alert;
