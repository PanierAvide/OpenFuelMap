let PriceTools = require("../model/PriceTools")
/*
 * This file is part of OpenFuelMap.
 *
 * OpenFuelMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * OpenFuelMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OpenFuelMap.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Station details view shows the details of a given station
 */
class StationDetails {
//CONSTRUCTOR
	/**
	 * Class constructor.
	 * @param {string} divId The DIV ID where the view must be created
	 */
	constructor(divId) {
		this.station = null;
		this.fuel = null;
		this.domElements = {};
		this.expanded = false;

		//Div cleaning
		this.dom = document.getElementById(divId);
		this.dom.innerHTML = '';
		this.dom.className = "ofm-station-details";

		//Toggle
		this.domElements.toggle = document.createElement("div");
		this.domElements.toggle.className = "ofm-station-details-toggle";
		this.domElements.toggle.innerHTML = "▲";
		this.domElements.toggle.onclick = this.toggle.bind(this);
		this.dom.appendChild(this.domElements.toggle);

		//Close
		this.domElements.close = document.createElement("span");
		this.domElements.close.className = "ofm-station-details-close";
		this.domElements.close.innerHTML = "x";
		this.domElements.close.onclick = () => { this.dom.className += " hidden"; };
		this.dom.appendChild(this.domElements.close);

		//Fuel panel
		this.domElements.fuel = document.createElement("div");
		this.domElements.fuel.className = "ofm-station-details-fuel";
		this.dom.appendChild(this.domElements.fuel);

		this.domElements.fuelName = document.createElement("h4");
		this.domElements.fuelName.className = "ofm-station-details-fuel-name";
		this.domElements.fuel.appendChild(this.domElements.fuelName);

		this.domElements.fuelPrice = document.createElement("span");
		this.domElements.fuelPrice.className = "ofm-station-details-fuel-price";
		this.domElements.fuel.appendChild(this.domElements.fuelPrice);
		this.domElements.fuelDate = document.createElement("span");
		this.domElements.fuelDate.className = "ofm-station-details-fuel-date";
		this.domElements.fuel.appendChild(this.domElements.fuelDate);

		//Station name
		this.domElements.name = document.createElement("h3");
		this.domElements.name.className = "ofm-station-details-name";
		this.dom.appendChild(this.domElements.name);

		//Station link
		this.domElements.geo = document.createElement("a");
		this.domElements.geo.setAttribute("target", "_blank");
		this.domElements.geo.className = "ofm-station-details-geo fa fa-solid fa-share-square-o";
		this.dom.appendChild(this.domElements.geo);

		//Clearer
		let clearer = document.createElement("div");
		clearer.className = "ofm-station-details-clearer";
		clearer.innerHTML = "&nbsp;";
		this.dom.appendChild(clearer);

		//Details
		this.domElements.details = document.createElement("div");
		this.domElements.details.className = "ofm-station-details-full hidden";
		this.dom.appendChild(this.domElements.details);

		this.domElements.editOsm = document.createElement("span");
		this.domElements.editOsm.className = "ofm-station-details-edit";
		this.domElements.editOsm.innerHTML = "✏️";
		this.domElements.details.appendChild(this.domElements.editOsm);

		let brand = document.createElement("h4");
		brand.innerHTML = "Brand";
		this.domElements.details.appendChild(brand);

		this.domElements.brand = document.createElement("p");
		this.domElements.brand.className = "ofm-station-details-text";
		this.domElements.details.appendChild(this.domElements.brand);

		let prices = document.createElement("h4");
		prices.innerHTML = "Prices";
		this.domElements.details.appendChild(prices);

		this.domElements.prices = document.createElement("div");
		this.domElements.prices.className = "ofm-station-details-full-prices";
		this.domElements.details.appendChild(this.domElements.prices);

// 		let services = document.createElement("h4");
// 		services.innerHTML = "Services";
// 		this.domElements.details.appendChild(services);
//
// 		this.domElements.services = document.createElement("div");
// 		this.domElements.services.className = "ofm-station-details-full-services";
// 		this.domElements.details.appendChild(this.domElements.services);
	}

	reset() {
		this.domElements.fuelPrice.innerHTML = ""
		this.domElements.fuelDate.innerHTML = ""
		this.domElements.fuelName.innerHTML = ""
		this.domElements.brand.innerHTML = ""
	}
//MODIFIERS
	/**
	 * Change the station to display
	 * @param {Station} station The station to display
	 */
	set(station) {
		this.reset()
		this.station = station;
		this.domElements.name.innerHTML = this.station.name;
		this.domElements.geo.setAttribute("href", geoLink(this.station.coordinates));
        if (this.station.prices[this.fuel]) {
			this.domElements.fuelName.innerHTML = this.fuel;
			const price = this.station.prices[this.fuel]
            this.domElements.fuelPrice.innerHTML = price.valeur;
            this.domElements.fuelDate.innerHTML =
					new Intl.DateTimeFormat().format(new Date(price.maj)) +
					" (" + new Intl.RelativeTimeFormat().format(Math.round(PriceTools.priceOld(price)), 'day') + ")";
        }
		this.domElements.brand.innerHTML = this.station.brand;


		if(this.station.osmId) {
			this.domElements.editOsm.onclick = () => { window.open("https://openstreetmap.org/"+this.station.osmId, "_blank"); };
			this.domElements.editOsm.style.display = "block";
		}
		else {
			this.domElements.editOsm.style.display = "none";
			this.domElements.editOsm.onclick = null;
		}

		//Prices
		this.domElements.prices.innerHTML = "";
		for(let p in this.station.prices) {
			let priceDiv = document.createElement("div");

			let label = document.createElement("span");
			label.innerHTML = p;
			priceDiv.appendChild(label);

			let price = document.createElement("span");
            if (this.station.prices[p]) {
                price.innerHTML = this.station.prices[p].valeur;
            }
			priceDiv.appendChild(price);

			this.domElements.prices.appendChild(priceDiv);
		}

		//Services
// 		this.domElements.services.innerHTML = "";
// 		for(let s of this.station.services) {
// 			let label = document.createElement("span");
// 			label.innerHTML = s;
// 			this.domElements.services.appendChild(label);
// 		}
	}

	/**
	 * Change the fuel to use for stations color
	 * @param {string} fuel The fuel to use
	 * @param {boolean} [reload] True if data must be updated
	 */
	setFuel(fuel, reload) {
		this.fuel = fuel;
		if(reload) {
			this.set(this.station);
		}
	}

	/**
	 * Expands or shrinks the view.
	 * @private
	 */
	toggle() {
		if(this.expanded) {
			this.domElements.toggle.innerHTML = "▲";
			this.domElements.details.className = "ofm-station-details-full hidden";
		}
		else {
			this.domElements.toggle.innerHTML = "▼";
			this.domElements.details.className = "ofm-station-details-full";
		}

		this.expanded = !this.expanded;
	}
}

function geoLink(coordinates) {
	if (window.matchMedia('(max-width: 768px)').matches)
		return `geo:${L.GeoJSON.latLngToCoords(coordinates).reverse().join(',')}`
	else
		return `https://www.openstreetmap.org/#map=19/${coordinates.lat}/${coordinates.lng}`
}

module.exports = StationDetails;
