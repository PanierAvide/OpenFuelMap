OpenFuelMap
=============

![OpenFuelMap logo](./src/img/logo.png)

Read-me
-------

OpenFuelMap is an interactive map showing fuel prices in France and Germany. OpenFuelMap is written in JavaScript (ES6).

A live demo is available at: https://openfuelmap.net/


Running
-------

If you want to run your own instance of OpenFuelMap, run the following commands :

```bash
git clone https://framagit.org/PanierAvide/OpenFuelMap.git
cd OpenFuelMap
npm install
vim config.json # Create configuration file
npm run build
npm run start
```

Then, website and API are running at `http://localhost:3000/`.


License
-------

Copyright 2016-2019 Adrien PAVIE

See LICENSE for complete AGPL3 license.

OpenFuelMap is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenFuelMap is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenFuelMap. If not, see <http://www.gnu.org/licenses/>.
