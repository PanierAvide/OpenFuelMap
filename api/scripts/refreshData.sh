#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export DISPLAY=:0.0

echo "-- Downloading --"
wget "https://donnees.roulez-eco.fr/opendata/instantane" -O "${ZIP_DATA}"
unzip -o "${ZIP_DATA}" -d "${DATA_DIR}"

set -e
echo "-- Convert data --"
xsltproc -o "${DATA_DIR}/carburants_raw1.geojson" "$(dirname "$0")/FR_stations_geojson.xslt" "${DATA_DIR}/PrixCarburants_instantane.xml"
cat "${DATA_DIR}/carburants_raw1.geojson" | grep -v "\[\,\]" > "${DATA_DIR}/carburants_raw.geojson"

echo "-- Clean --"
psql -c "drop table if exists carburants"
psql -c "drop table if exists carburants2"
psql -c "drop table if exists stations"

echo "-- Conflation --"
sed -i 's/ref:fr:prix-carburants/ref:FR:prix-carburants/g' ${DATA_DIR}/stations.geojson
ogr2ogr -f "PostgreSQL" PG:"${OGR2OGR_PG}" -overwrite -nln stations ${DATA_DIR}/stations.geojson
ogr2ogr -f "PostgreSQL" PG:"${OGR2OGR_PG}" -overwrite -nln carburants "${DATA_DIR}/carburants_raw.geojson"

psql -c "UPDATE carburants SET wkb_geometry = ST_SetSRID(ST_Point(ST_X(wkb_geometry)/100000, ST_Y(wkb_geometry)/100000), 4326)"
psql -c "UPDATE carburants SET prices = regexp_replace(prices::text, '\.(\d{3})\d+', '.\1', 'g')::json"
psql -c "DROP TABLE IF EXISTS carburants2; CREATE TABLE carburants2 AS SELECT c.id, COALESCE(s.name, '') AS name, COALESCE(s.brand, s.operator, '') AS brand, c.services, c.prices, COALESCE(s.id, '') AS osm_id, CASE WHEN s.wkb_geometry IS NOT NULL THEN s.wkb_geometry ELSE c.wkb_geometry END AS wkb_geometry FROM carburants c LEFT JOIN stations s ON c.id = s.\"ref:fr:prix_carburants\""
psql -c "CREATE INDEX carburants_wkb_geometry_idx ON carburants2 USING GIST(wkb_geometry)"

ogr2ogr -f "GeoJSON" "${DATA_DIR}/carburants.geojson" PG:"${OGR2OGR_PG}" "carburants2(wkb_geometry)" -lco coordinate_precision=5
ogr2ogr -f "GeoJSON" "${DATA_DIR}/carburants_gouv.geojson" PG:"${OGR2OGR_PG}" "carburants(wkb_geometry)" -lco coordinate_precision=5

cp "${DATA_DIR}/"* "${DATA_EXPORT_DIR}"