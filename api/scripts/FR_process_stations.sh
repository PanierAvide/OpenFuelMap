#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export DISPLAY=:0.0

#
# Update OpenFuelMap files on FTP
# To retrieve stations.geojson, use http://overpass-turbo.eu/s/N3B and export as GeoJSON
#

# Constants
DATA_DIR="/tmp/carburants"
ZIP_DATA="${DATA_DIR}/data.zip"
HOST="my.ftp.com"
FOLDER="/depot/remote"
USER="ftpuser"
PASS="ftppass"
PGPORT="5433"

echo "-- Cleaning --"
rm -rf "${DATA_DIR}"
mkdir -p "${DATA_DIR}"

echo "-- Downloading --"
wget "https://donnees.roulez-eco.fr/opendata/instantane" -O "${ZIP_DATA}"
unzip "${ZIP_DATA}" -d "${DATA_DIR}"

set -e
echo "-- Convert data --"
xsltproc -o "${DATA_DIR}/carburants_raw1.geojson" ./FR_stations_geojson.xslt "${DATA_DIR}/PrixCarburants_instantane.xml"
cat "${DATA_DIR}/carburants_raw1.geojson" | grep -v "\[\,\]" > "${DATA_DIR}/carburants_raw.geojson"

echo "-- Create DB --"
set +e
psql -U ofm -p "${PGPORT}" -d openfuelmap -c "SELECT 1+1"
if [ "$?" -eq "0" ]; then
	psql -U ofm -p "${PGPORT}" -d openfuelmap -c "DROP TABLE IF EXISTS carburants"
else
	psql -U ofm -p "${PGPORT}" -c "DROP DATABASE IF EXISTS openfuelmap"
	psql -U ofm -p "${PGPORT}" -c "CREATE DATABASE openfuelmap"
	psql -U ofm -p "${PGPORT}" -d openfuelmap -c "CREATE EXTENSION postgis"
fi
set -e

echo "-- Conflation --"
sed -i 's/ref:fr:prix-carburants/ref:FR:prix-carburants/g' ./stations.geojson
ogr2ogr -f "PostgreSQL" PG:"dbname=openfuelmap user=ofm port=${PGPORT}" -overwrite -nln stations ./stations.geojson
ogr2ogr -f "PostgreSQL" PG:"dbname=openfuelmap user=ofm port=${PGPORT}" -overwrite -nln carburants "${DATA_DIR}/carburants_raw.geojson"

psql -U ofm -p "${PGPORT}" -d openfuelmap -c "UPDATE carburants SET wkb_geometry = ST_SetSRID(ST_Point(ST_X(wkb_geometry)/100000, ST_Y(wkb_geometry)/100000), 4326)"
psql -U ofm -p "${PGPORT}" -d openfuelmap -c "UPDATE carburants SET prices = regexp_replace(prices::text, '\.(\d{3})\d+', '.\1', 'g')::json"
psql -U ofm -p "${PGPORT}" -d openfuelmap -c "DROP TABLE IF EXISTS carburants2; CREATE TABLE carburants2 AS SELECT c.id, COALESCE(s.name, '') AS name, COALESCE(s.brand, s.operator, '') AS brand, c.services, c.prices, COALESCE(s.id, '') AS osm_id, CASE WHEN s.wkb_geometry IS NOT NULL THEN s.wkb_geometry ELSE c.wkb_geometry END AS wkb_geometry FROM carburants c LEFT JOIN stations s ON c.id = s.\"ref:fr:prix_carburants\""
psql -U ofm -p "${PGPORT}" -d openfuelmap -c "CREATE INDEX carburants_wkb_geometry_idx ON carburants2 USING GIST(wkb_geometry)"

ogr2ogr -f "GeoJSON" "${DATA_DIR}/carburants.geojson" PG:"dbname=openfuelmap user=ofm port=${PGPORT}" "carburants2(wkb_geometry)" -lco coordinate_precision=5
ogr2ogr -f "GeoJSON" "${DATA_DIR}/carburants_gouv.geojson" PG:"dbname=openfuelmap user=ofm port=${PGPORT}" "carburants(wkb_geometry)" -lco coordinate_precision=5

sed -i 's#, "name": ""##g;s#, "brand": ""##g;s#, "osm_id": ""##g' "${DATA_DIR}/carburants.geojson"

echo "-- Uploading --"
ncftpput -u $USER -p $PASS -R $HOST "$FOLDER" "${DATA_DIR}/carburants.geojson"
ncftpput -u $USER -p $PASS -R $HOST "$FOLDER" "${DATA_DIR}/carburants_gouv.geojson"

echo "-- Cleaning --"
rm -rf "${DATA_DIR}"

echo "-- Done --"
