<!DOCTYPE stylesheet [
	<!ENTITY newln "&#xA;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" encoding="utf-8"/>

	<xsl:template match="/pdv_liste">
		<xsl:text>{ "type": "FeatureCollection", "features": [&newln;</xsl:text>
		<xsl:apply-templates select="pdv"/>
		<xsl:text>] }</xsl:text>
	</xsl:template>

	<xsl:template match="pdv">
		<xsl:text>{ "type": "Feature", "id": "</xsl:text>
		<xsl:value-of select="@id"/>

		<xsl:text>", "geometry": { "type": "Point", "coordinates": [</xsl:text>
		<xsl:value-of select="@longitude"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="@latitude"/>

		<xsl:text>] }, "properties": {</xsl:text>
		<xsl:text>"addr": "</xsl:text>
		<xsl:call-template name="encode-string"><xsl:with-param name="s" select="adresse"/></xsl:call-template>
		<xsl:text>", "postcode": "</xsl:text>
		<xsl:value-of select="@cp"/>
		<xsl:text>", "city": "</xsl:text>
		<xsl:call-template name="encode-string"><xsl:with-param name="s" select="ville"/></xsl:call-template>
		<xsl:text>", "services": [ </xsl:text>
		<xsl:apply-templates select="services/service"/>
		<xsl:text> ], "prices": { </xsl:text>
		<xsl:apply-templates select="prix"/>
		<xsl:text> } } }</xsl:text>
		<xsl:if test="following-sibling::pdv">,</xsl:if>
		<xsl:text>&newln;</xsl:text>
	</xsl:template>

	<xsl:template match="service">
		<xsl:text>"</xsl:text>
		<xsl:value-of select="normalize-space(.)" />
		<xsl:text>"</xsl:text>
		<xsl:if test="following-sibling::service">, </xsl:if>
	</xsl:template>

	<xsl:template match="prix">
		<xsl:text>"</xsl:text>
		<xsl:value-of select="@nom" />
		<xsl:text>": { "valeur":</xsl:text>
		<xsl:value-of select="@valeur" />
        <xsl:text>, "maj":"</xsl:text>
		<xsl:value-of select="@maj" />
        <xsl:text>"} </xsl:text>
		<xsl:if test="following-sibling::prix">,</xsl:if>
	</xsl:template>


	<xsl:template name="encode-string">
		<xsl:param name="s" select="''"/>
		<xsl:param name="encoded" select="''"/>

		<xsl:choose>
			<xsl:when test="$s = ''">
				<xsl:value-of select="$encoded"/>
			</xsl:when>
			<xsl:when test="contains($s, '&quot;')">
				<xsl:call-template name="encode-string">
					<xsl:with-param name="s" select="substring-after($s,'&quot;')"/>
					<xsl:with-param name="encoded"
						select="concat($encoded,substring-before($s,'&quot;'),'\&quot;')"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($encoded, $s)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>

