/**
 * A fetcher is an object allowing to get fuel prices data from a specific provider.
 * It is associated to a specific area.
 */
class Fetcher {
	/**
	 * Class constructor
	 * @param {string} areaId The area ID (mostly country code like FR, DE)
	 * @param {string} attribution The attribution for dataset
	 */
	constructor(areaId, attribution) {
		this.areaId = areaId;
		this.attribution = attribution;
	}

	/**
	 * List stations and their prices in given area
	 * @param {number[]} bbox The bounding box, as [ lat1,lng1,lat2,lng2 ]
	 * @return {Promise} Resolves on stations data, as a GeoJSON feature collection
	 */
	getStations(bbox) {
		throw new Error("Should be overidden");
	}

	/**
	 * Transforms an object into a query string for URL
	 * @param {Object} params List of URL parameters
	 * @return {string} The parameters into query string format
	 */
	paramsToQueryString(params) {
		return Object.entries(params).map(e => e[0]+"="+e[1]).join("&");
	}
}

module.exports = Fetcher;
