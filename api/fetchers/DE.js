const Fetcher = require("./Fetcher");
const CONFIG = require("../../config.json");
const request = require("request-promise-native");
const distance = require("@turf/distance").default;

/**
 * Fetcher for Germany (DE).
 */
class DE extends Fetcher {
	constructor() {
		super("DE", "Tankerkoenig");
		this._apiKey = CONFIG.DE_tankerkonig_api;
		this._apiUrl = "https://creativecommons.tankerkoenig.de/json";
	}

	getStations(bbox) {
		const center = [ (bbox[1] + bbox[3]) / 2, (bbox[0] + bbox[2]) / 2 ];
		const dist = distance(center, [ bbox[1], bbox[0] ], { units: "kilometers" });
		const params = {
			lat: center[1],
			lng: center[0],
			rad: Math.min(Math.ceil(dist), 25),
			type: "all",
			apikey: this._apiKey
		};

		const url = this._apiUrl + "/list.php?" + this.paramsToQueryString(params);
		return request(url)
			.then(data => {
				const result = { attribution: this.attribution };
				data = JSON.parse(data);

				if(data.status === "ok" && data.stations && data.stations.length >= 0) {
					result.status = "ok";
					result.stations = data.stations.map(s => ({
						type: "Feature",
						id: this.areaId+"_"+s.id,
						properties: {
							name: s.name,
							brand: s.brand,
							prices: {
								"SP95 (E5)": s.e5,
								"E10": s.e10,
								"Diesel": s.diesel
							},
							price_unit: "€"
						},
						geometry: {
							type: "Point",
							coordinates: [s.lng, s.lat]
						}
					}));
				}
				else {
					result.status = "failed";
					result.reason = "Invalid data received from API";
				}

				return result;
			})
			.catch(e => {
				console.log(e);
				return { attribution: this.attribution, status: "failed", reason: e.message };
			});
	}
}

module.exports = DE;
