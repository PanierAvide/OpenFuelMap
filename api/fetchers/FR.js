const Fetcher = require("./Fetcher");
const { Pool } = require('pg');

/**
 * Fetcher for France (FR).
 */
class FR extends Fetcher {
	constructor() {
		super("FR", "Ministère Économie + OSM");

		// Create pool of connections
		this.pool = new Pool();
	}

	getStations(bbox) {
		const poly = "POLYGON(("+bbox[1]+" "+bbox[0]+","+bbox[3]+" "+bbox[0]+","+bbox[3]+" "+bbox[2]+","+bbox[1]+" "+bbox[2]+","+bbox[1]+" "+bbox[0]+"))";
		return this.pool
			.query("SELECT id, name, brand, services, prices::json, osm_id, ST_X(wkb_geometry) AS lng, ST_Y(wkb_geometry) AS lat FROM carburants2 WHERE wkb_geometry && ST_GeomFromText($1)", [poly])
			.then(res => {
				return { status: "ok", attribution: this.attribution, stations: res.rows.map(s => ({
					type: "Feature",
					id: this.areaId+"_"+s.id,
					properties: {
						name: s.name,
						brand: s.brand,
						prices: {
							"E10": s.prices.E10,
							"E85": s.prices.E85,
							"Diesel": s.prices.Gazole,
							"GPLc": s.prices.GPLc,
							"SP95 (E5)": s.prices.SP95,
							"SP98": s.prices.SP98,
						},
						price_unit: "€",
						services: s.services,
						osm_id: s.osm_id
					},
					geometry: {
						type: "Point",
						coordinates: [s.lng, s.lat]
					}
				})) };
			})
			.catch(e => {
				return { status: "failed", reason: e };
			});
	}
}

module.exports = FR;
