/**
 * Main code of API
 */

const express = require("express");
const compression = require("compression");
const fs = require("fs");
const booleanIntersects = require("@turf/boolean-intersects").default;
const flatten = require("array-flatten").flatten;
const boundaries = require("./boundaries.json");
const FetcherFR = require("./fetchers/FR");
const FetcherDE = require("./fetchers/DE");

// Init API
const app = express();
app.use(compression());
const port = process.env.PORT || 3000;

// Init fetchers
const fetchers = {
	"FR": new FetcherFR(),
	"DE": new FetcherDE()
};

// Log queries
app.use((req, res, next) => {
	const toLog = {
		ip: req.ip,
		path: req.path,
		query: req.query,
		ts: Date.now()
	};

	fs.appendFile('api.log', JSON.stringify(toLog)+'\n', (err) => {
		if(err) {
			console.log("Can't write into log file", err);
		}
	});

	next();
});

// Website
app.use('/', express.static(__dirname+'/../build'));

// Stations
app.get('/stations', (req, res) => {
	try {
		// Check bbox parameter
		if(!req.query.bbox || !/^(-?\d+(\.\d+)?)(,(-?\d+(\.\d+)?)){3}$/.test(req.query.bbox)) {
			throw new Error("bbox parameter should be a list of 4 coordinates : lat1,lng1,lat2,lng2");
		}

		const bbox = req.query.bbox.split(",").map(c => parseFloat(c));
		const bboxGeom = { type: "Polygon", coordinates: [[ [ bbox[1], bbox[0] ], [ bbox[3], bbox[2] ] ]] };

		try {
			// Filter boundaries according to area
			const areas = boundaries.features.filter(b => booleanIntersects(b, bboxGeom)).map(b => b.properties.id);
			const geojson = { type: "FeatureCollection", features: [] };

			// Empty result
			if(areas.length === 0) {
				geojson.status = "ok";
				res.send(geojson);
			}
			// One or several areas to fetch
			else {
				Promise.all(areas.map(a => fetchers[a].getStations(bbox)))
				.then(results => {
					const statuses = [...new Set(results.map(r => r.status))];
					const status = statuses.length === 1 ? statuses[0] : (statuses.indexOf("ok") >= 0 ? "partial" : "failed");

					if(status === "failed") {
						console.log("Errors: "+results.map(r => r.reason).join(" | "));
						res.status(500).send("Failed to get data");
					}
					else {
						const stations = flatten(results.map(r => r.stations || []));
						const attributions = results.map(r => r.attribution).join(", ");

						geojson.status = status;
						geojson.features = stations.map(v => enhance(v));
						geojson.attribution = attributions;
						geojson.fuels = carburantOrder;
						res.send(geojson);
					}
				});
			}
		}
		catch(e) {
			res.status(500).send("Error during process of boundaries "+e.message);
		}
	}
	catch(e) {
		res.status(400).send("Invalid request "+e.message);
	}
});
app.get('/fuels', (req, res) => {
	res.send(
		{
			fuels: carburantOrder
		})
})

const carburantOrder = ["GPLc", "Diesel", "E85", "SP95 (E5 ou E10)", "SP95 (E5)", "SP95 (E10)", "SP98"]

function enhance(station) {
	if (station.properties.prices) {
		const prices = new Map(Object.entries(station.properties.prices))
		const e10 = prices.get('E10')
		const e5 = prices.get('SP95 (E5)')
		prices.delete('E10')
		if (e10)
			prices.set('SP95 (E10)', e10)
		const sp95 = [e10, e5].filter(Boolean).sort((a, b) => a.valeur > b.valeur ? 1 : -1)[0]
		prices.set('SP95 (E5 ou E10)', sp95)

		const newPrice = new Map()
		carburantOrder.forEach(c => newPrice.set(c, prices.get(c)))

		return {...station, properties:{...station.properties, prices: Object.fromEntries(Array.from(newPrice.entries()))}};
	} else
		return station;
}

// 404
app.use((req, res) => {
	res.status(404).send(req.originalUrl + ' not found')
});

// Start
app.listen(port, () => {
	console.log('API started on port: ' + port);
});
